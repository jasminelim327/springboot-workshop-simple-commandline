package com.iclchan.workshop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringBootApplication implements CommandLineRunner {
    private final Logger log = LoggerFactory.getLogger(SimpleSpringBootApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SimpleSpringBootApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("Hello world");
    }
}
